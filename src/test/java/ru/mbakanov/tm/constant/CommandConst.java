package ru.mbakanov.tm.constant;

public interface CommandConst {

    String HELP = "help";

    String ABOUT = "about";

    String VERSION = "version";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

}
