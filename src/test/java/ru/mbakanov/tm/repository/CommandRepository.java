package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.api.repository.ICommandRepository;
import ru.mbakanov.tm.constant.ArgumentConst;
import ru.mbakanov.tm.constant.CommandConst;
import ru.mbakanov.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP, "Show terminal`s commands."
    );

    public static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    public static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION, "Show application version."
    );

    public static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO, "Show system hardware info."
    );

    public static final Command EXIT = new Command(
            CommandConst.EXIT, null ,"Close application."
    );

    public static final Command COMMAND = new Command(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS ,"Show program commands."
    );

    public static final Command ARGUMENT = new Command(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS ,"Show program arguments."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands (Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs (Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
