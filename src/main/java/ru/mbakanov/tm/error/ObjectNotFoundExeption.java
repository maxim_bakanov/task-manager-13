package ru.mbakanov.tm.error;

public class ObjectNotFoundExeption extends RuntimeException {

    public ObjectNotFoundExeption(String value) {
        super("Error! " + value + " is not found...");
    }

}
