package ru.mbakanov.tm.error;

public class IdEmptyExeption extends RuntimeException {

    public IdEmptyExeption(){
        super("Error! Id is empty...");
    }

}
