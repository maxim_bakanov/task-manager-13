package ru.mbakanov.tm.error;

public class NameEmptyExeption extends RuntimeException {

    public NameEmptyExeption(){
        super("Error! Name is empty...");
    }

}
