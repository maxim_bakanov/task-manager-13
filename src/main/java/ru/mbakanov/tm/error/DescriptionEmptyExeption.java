package ru.mbakanov.tm.error;

public class DescriptionEmptyExeption extends RuntimeException {

    public DescriptionEmptyExeption(){
        super("Error! Description is empty...");
    }

}
