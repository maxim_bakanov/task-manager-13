package ru.mbakanov.tm.error;

public class IndexIncorrectExeption extends RuntimeException{

    public IndexIncorrectExeption() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectExeption(String value) {
        super("Error! The '" + value + "' is not a number...");
    }


}
