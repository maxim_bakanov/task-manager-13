package ru.mbakanov.tm.error;

public class UnexpectedCommandExeption extends RuntimeException {

    public UnexpectedCommandExeption(String value) {
        super("Error! '" + value + "' is an unexpected command. Use 'help' to see task manager command list.");
    }

}
