package ru.mbakanov.tm;

import ru.mbakanov.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
