package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.repository.ITaskRepository;
import ru.mbakanov.tm.api.service.ITaskService;
import ru.mbakanov.tm.error.*;
import ru.mbakanov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository ITaskRepository) {
        this.taskRepository = ITaskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyExeption();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new ObjectNotFoundExeption("Task");
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new ObjectNotFoundExeption("Task");
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clean() {
        taskRepository.clean();
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return taskRepository.findOneByName(id);
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Task task = findOneById(id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateOneByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Task task = findOneByIndex(index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        task.setName(name);
        task.setDescription(description);
        return task;
    }
}
