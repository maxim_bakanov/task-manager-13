package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.repository.IProjectRepository;
import ru.mbakanov.tm.error.*;
import ru.mbakanov.tm.model.Project;

import java.util.List;

public class ProjectService implements ru.mbakanov.tm.api.service.IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyExeption();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ObjectNotFoundExeption("Project");
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ObjectNotFoundExeption("Project");
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clean() {
        projectRepository.clean();
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return projectRepository.findOneByName(id);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Project project = findOneById(id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        if (name == null || name.isEmpty()) throw new NameEmptyExeption();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
