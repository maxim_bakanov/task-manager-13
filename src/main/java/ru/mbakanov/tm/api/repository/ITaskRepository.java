package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.model.Task;

import java.util.List;

public interface  ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clean();

    Task findOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneById(String id);

    Task removeOneByName(String name);

    Task removeOneByIndex(Integer index);

}
