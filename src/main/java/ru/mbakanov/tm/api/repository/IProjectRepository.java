package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clean();

    Project findOneById(String id);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    Project removeOneById(String id);

    Project removeOneByName(String name);

    Project removeOneByIndex(Integer index);

}
