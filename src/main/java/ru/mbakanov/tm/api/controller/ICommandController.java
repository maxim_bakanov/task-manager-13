package ru.mbakanov.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showVersion();

    void showAbout();

    void showInfo();

    void showCommands();

    void showArguments();

    void exit();

}
