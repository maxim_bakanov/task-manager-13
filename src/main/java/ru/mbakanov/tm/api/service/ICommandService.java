package ru.mbakanov.tm.api.service;

import ru.mbakanov.tm.model.Command;

public interface ICommandService {

    String[] getCommands();

    String[] getArgs();

    Command[] getTerminalCommands();

}
