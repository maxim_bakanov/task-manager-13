package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ru.mbakanov.tm.api.repository.IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clean() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index-1);
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        remove(project);
        return project;
    }

}
