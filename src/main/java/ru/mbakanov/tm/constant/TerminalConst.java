package ru.mbakanov.tm.constant;

public interface TerminalConst {

    String VERSION_NUM = "1.0.9";

    String DEV_NAME = "Name: Maxim Bakanov";

    String DEV_EMAIL = "E-Mail: graonnirvald@gmail.com";

}
