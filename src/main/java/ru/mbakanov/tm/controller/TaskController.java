package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.service.ITaskService;
import ru.mbakanov.tm.api.controller.ITaskController;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.model.Task;
import ru.mbakanov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService ITaskService) {
        this.taskService = ITaskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clean();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[enter name:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description:]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[OK]");
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[OK]");
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[OK]");
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdater = taskService.updateOneById(id, name, description);
        if (taskUpdater == null) throw new ObjectNotFoundExeption("Updated task");
        System.out.println("[OK]");
        showTask(taskUpdater);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdater = taskService.updateOneByIndex(index, name, description);
        if (taskUpdater == null) throw new ObjectNotFoundExeption("Updated task");
        System.out.println("[OK]");
        showTask(taskUpdater);
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        else System.out.println("[OK]");
    }
}
