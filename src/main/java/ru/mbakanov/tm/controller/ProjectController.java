package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.service.IProjectService;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.model.Project;
import ru.mbakanov.tm.model.Task;
import ru.mbakanov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements ru.mbakanov.tm.api.controller.IProjectController {

    private IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " +  project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clean();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[enter name:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description:]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[OK]");
        showProject(project);
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[OK]");
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[OK]");
        showProject(project);
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdater = projectService.updateOneById(id, name, description);
        if (projectUpdater == null) throw new ObjectNotFoundExeption("Updated project");
        System.out.println("[OK]");
        showProject(projectUpdater);
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdater = projectService.updateOneByIndex(index, name, description);
        if (projectUpdater == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        showProject(projectUpdater);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        else System.out.println("[OK]");
    }
}
